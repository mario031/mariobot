class LineClient
  END_POINT = "https://api.line.me"

  def initialize(channel_access_token, proxy = nil)
    @channel_access_token = channel_access_token
    @proxy = proxy
  end

  def get(path)
    client = Faraday.new(:url => END_POINT) do |conn|
      conn.request :url_encoded
      conn.response :json, :content_type => /\bjson$/
      conn.adapter Faraday.default_adapter
    end

    res = client.get do |request|
      request.url path
      request.headers = {
        'Content-type' => 'application/json',
        'Authorization' => "Bearer #{@channel_access_token}"
      }
    end
    res
  end

  def post(path, data)
    client = Faraday.new(:url => END_POINT) do |conn|
      conn.request :json
      conn.response :json, :content_type => /\bjson$/
      conn.adapter Faraday.default_adapter
      conn.proxy @proxy
    end

    res = client.post do |request|
      request.url path
      request.headers = {
        'Content-type' => 'application/json',
        'Authorization' => "Bearer #{@channel_access_token}"
      }
      request.body = data
    end
    res
  end

  #################
  ##  LINE api群
  #################
  def profile()
    get("/v2/profile/")
  end

  ## text用のreply ##
  def reply(replyToken, text)

    messages = [
      {
        "type" => "text" ,
        "text" => text
      }
    ]

    body = {
      "replyToken" => replyToken ,
      "messages" => messages
    }
    post('/v2/bot/message/reply', body.to_json)
  end

  ## location用のreply
  def reply_location(replyToken, lat, lon, api_id, keyword)
    ## Hotpepperグルメよう ##
    if api_id.to_i == 1

      columns = []

      h = Hotpepper.find_by_location(lat, lon, keyword)
      shops = h.attributes['shop']
      shops.each do |s|
        image = s.attributes["photo"].attributes["pc"].attributes["l"]
        title = s.attributes["name"]
        text = s.attributes["catch"]
        time = s.attributes["open"]
        split_text = "#{text}".scan(/.{1,60}/)
        detail = s.attributes["urls"].attributes["pc"]
        column = {"thumbnailImageUrl" => image,
                  "title" => title,
                  "text" => split_text[0],
                  "actions" => [
                      {
                        "type" => "uri",
                        "label" => "詳細を見る",
                        "uri" => detail
                      }
                    ]
                }
        columns.push(column)
      end

      messages = [
        {
          "type" => "template",
          "altText" => "Hot Pepper グルメ",
          "template" => {
            "type" => "carousel",
             "columns" => columns
          }
        }
      ]

      body = {
        "replyToken" => replyToken ,
        "messages" => messages
      }
      if columns[0].blank?
        self.reply(replyToken, "近くにお店はありません")
      else
        post('/v2/bot/message/reply', body.to_json)
      end
    elsif api_id.to_i == 4
       ## その他 ##
      columns = []

      h = Gnavi.find_by_location(lat, lon, keyword)
      if h.attributes["total_hit_count"].to_i > 1
        rests = h.attributes['rest']
        rests.each do |r|
        image = r.attributes["image_url"].attributes["shop_image1"].to_s
        if image.include?("Gnavi")
          image = "https://life-cloud.ht.sfc.keio.ac.jp/~mario/noimage.jpg"
        end
        title = r.attributes["name"]
        text = r.attributes["pr"].attributes["pr_short"].to_s.gsub("<BR>", "")
        if text.include?("Gnavi")
          text = "No Data"
        end
        # time = s.attributes["open"]
        split_text = "#{text}".scan(/.{1,60}/)
        detail = r.attributes["url"]
        column = {"thumbnailImageUrl" => image,
                  "title" => title || "No Title",
                  "text" => split_text[0] || "Not text",
                  "actions" => [
                      {
                        "type" => "uri",
                        "label" => "詳細を見る",
                        "uri" => detail
                      }
                    ]
                }
        columns.push(column)
        end
      elsif h.attributes["total_hit_count"].to_i == 1
        r = h.attributes['rest']
        image = r.attributes["image_url"].attributes["shop_image1"].to_s
        if image.include?("Gnavi")
          image = "https://life-cloud.ht.sfc.keio.ac.jp/~mario/noimage.jpg"
        end
        title = r.attributes["name"]
        text = r.attributes["pr"].attributes["pr_short"].to_s.gsub("<BR>", "")
        if text.include?("Gnavi")
          text = "No Data"
        end
        # time = s.attributes["open"]
        split_text = "#{text}".scan(/.{1,60}/)
        detail = r.attributes["url"]
        column = {"thumbnailImageUrl" => image || "https://life-cloud.ht.sfc.keio.ac.jp/~mario/noimage.jpg",
                  "title" => title || "No Title",
                  "text" => split_text[0] || "Not text",
                  "actions" => [
                      {
                        "type" => "uri",
                        "label" => "詳細を見る",
                        "uri" => detail
                      }
                    ]
                }
        columns.push(column)
      end
      messages = [
        {
          "type" => "template",
          "altText" => "ぐるなび",
          "template" => {
            "type" => "carousel",
             "columns" => columns
          }
        }
      ]

      body = {
        "replyToken" => replyToken ,
        "messages" => messages
      }
      if h.attributes["total_hit_count"].to_i == 0
        self.reply(replyToken, "近くに{#{keyword}}のお店はありません")
      else
        post('/v2/bot/message/reply', body.to_json)
      end
    else
    end
  end

  def push(user_id, text)

    messages = [
      {
        "type" => "text" ,
        "text" => text
      }
    ]

    body = {
      "to" => user_id ,
      "messages" => messages
    }
    post('/v2/bot/message/push', body.to_json)
  end
end


