Rails.application.routes.draw do

  	devise_for :users, controllers: {
  		omniauth_callbacks: "omniauth_callbacks"
	}
	root "bots#index"

  	post '/callback' => 'webhook#callback'
  	get '/push' => 'webhook#push'
  	get 'bots/select'
  	post 'bots/new'
 	resources :bots, :only => [:show, :index, :new, :create, :edit, :update, :destroy]
end
