class Hotpepper < ActiveResource::Base
	self.site = "http://webservice.recruit.co.jp/"
  	self.format = :json
  	api = ApiList.find_by(api: "hotpepper グルメ")
  	APIKEY = api.api_key

  	def self.find_by_location(lat, lon, keyword = "")
    	self.find(:one, from: '/hotpepper/gourmet/v1/',
      	params: {key: APIKEY, format: 'json', genre: keyword, lat: lat, lng: lon, count: 5, order: 4})
  	end
end
