class Gnavi < ActiveResource::Base
	self.site = "https://api.gnavi.co.jp"
  	
  	api = ApiList.find_by(api: "ぐるなび")
  	APIKEY = api.api_key

  	class Format
    include ActiveResource::Formats::JsonFormat
    	def decode(json)
      		super(json.gsub("@","_"))
    	end
 	end

  	self.format = Format.new
  	self.logger = Logger.new(Rails.root.join('log/production.log'))
  	def self.find_by_location(lat, lon, freeword = "")
    	self.find(:one, from: '/RestSearchAPI/20150630/', 
    	params: {keyid: APIKEY, format: 'json', freeword: freeword.encode("UTF-8"), latitude: lat, longitude: lon, range: 3, hit_per_page: 5})
  	end
end
