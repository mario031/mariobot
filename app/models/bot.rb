class Bot < ApplicationRecord	
	validates :text, presence: true
	validates :value, presence: true
end
