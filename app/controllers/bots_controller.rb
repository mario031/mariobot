class BotsController < ApplicationController
  http_basic_authenticate_with :name => "mario", :password => "bot" if Rails.env == "production"
  before_action :authorize

    def authorize
      return redirect_to user_line_omniauth_authorize_path unless user_signed_in?
    end

	  # def show
   #  	@bot = Bot.find(params[:id])
   #    if @bot.uid == current_user.uid
   #    else
   #      redirect_to bots_path
   #      flash[:alert] = "無効なユーザ"
   #    end
 	 #  end

 	  def index
    	@bots = Bot.where(uid: current_user.uid)
  	end

    def select
      @apis = ApiList.all
      @bot = Bot.new
    end

  	def new
      @id = params[:api_id]
      if @id.blank?
        redirect_to bots_select_path
        flash[:alert] = "出力値を選択してください"
      else
    	 @bot = Bot.new
       @api = ApiList.find(@id.to_i)
      end
  	end

  	def create
      b = Bot.find_by(text: params[:bot][:text], uid: current_user.uid)
      @bot = Bot.new(params.require(:bot).permit(:text, :value, :api_id, :deadline, :keyword).merge(:uid => current_user.uid))
      if b.blank?
        if @bot.save
          redirect_to bots_path
          flash[:notice] = "Botを作成しました"
        else
          redirect_to bots_select_path
          flash[:alert] = "入力値または出力値が入力されていません"
        end
        
      else
        redirect_to bots_select_path
        flash[:alert] = "そのkeyはすでに存在します"
      end
	  end

  	def edit
    	@bot = Bot.find(params[:id])
      @api = ApiList.find(@bot.api_id.to_i)
      if @bot.uid == current_user.uid
      else
        redirect_to bots_path
        flash[:alert] = "無効なユーザ"
      end
  	end

  	def update
    	@bot = Bot.find(params[:id])
      if @bot.uid == current_user.uid
        b = Bot.find_by(text: params[:bot][:text], uid: current_user.uid)
        if b.blank? || @bot.text == params[:bot][:text]
          if @bot.update(params.require(:bot).permit(:text, :value, :deadline, :keyword))
            redirect_to bots_path
            flash[:notice] = "Botを編集しました"
          else
            redirect_to edit_bot_path
            flash[:alert] = "入力値または出力値が入力されていません"
          end
        else
          redirect_to edit_bot_path
          flash[:alert] = "そのkeyはすでに存在します"
        end
      else
        redirect_to bots_path
        flash[:alert] = "無効なユーザ"
      end
  	end

  	def destroy
    	@bot = Bot.find(params[:id])
      if @bot.uid == current_user.uid
         @bot.destroy
        redirect_to bots_path
        flash[:alert] = "Botを削除しました"
      else
        redirect_to bots_path
        flash[:alert] = "無効なユーザ"
      end
	  end
end
