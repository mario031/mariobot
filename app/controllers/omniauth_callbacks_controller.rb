class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def line; basic_action end

  private

  def basic_action
    @omniauth = request.env['omniauth.auth']
    credentials = @omniauth['credentials']
    client = LineClient.new(credentials["token"])
    res = client.profile()
    if @omniauth.present?
      @profile = SocialProfile.where(provider: @omniauth['provider'], uid: res.body["userId"]).first
      if @profile
        @profile.set_values(@omniauth)
        sign_in(:user, @profile.user)
      else
        @profile = SocialProfile.new(provider: @omniauth['provider'], uid: res.body["userId"])
        email = @omniauth['info']['email'] ? @omniauth['info']['email'] : Faker::Internet.email
        @profile.user = current_user || User.create!(email: email, name: @omniauth['info']['name'], password: Devise.friendly_token[0, 20], uid: res.body["userId"])
        @profile.set_values(@omniauth)
        sign_in(:user, @profile.user)
        redirect_to bots_path and return
      end
    end
    redirect_to root_path
  end
end