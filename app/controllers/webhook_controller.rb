
class WebhookController < ApplicationController
  # Lineからのcallbackか認証
  protect_from_forgery with: :null_session # CSRF対策無効化

  CHANNEL_ID = "1509982064"
  CHANNEL_SECRET = "f129dfbd60e5dd84f12f6ecb342dbd08"
  CHANNEL_MID = "U26d8f7f2861e8d9c4d548b684c7d27cd"
  CHANNEL_ACCESS_TOKEN = "4ebuWYv6GddsBeceoOfwqGBCGSA3Rg2aPYTSKqjrUv0ykSa8ZwaFVFe2Y4+UIHuYwY63TGyxnkOU+IvnV7AW5SD+bz6pufZ4ITloVGycW04YwjgPL1+IyBXImEdpV/0PH8JWliEmwlumJ6KakOYYLgdB04t89/1O/w1cDnyilFU="

  def callback
    unless is_validate_signature
      render :nothing => true, status: 470
    end

    event = params["events"][0]
    # event_type = event["type"]
    replyToken = event["replyToken"]
    user_id = event["source"]["userId"]
    user_type = event["source"]["type"]
    message = event["message"]

    ####----位置情報が送られてきた場合cookieに保存数----####
    if !message["latitude"].blank?
      user = User.find_by(uid: user_id)
      user.update(lat: message["latitude"].to_f, lon: message["longitude"].to_f)
    end
    check(replyToken, message, user_id, user_type) 

    render :nothing => true, status: :ok
  end

  def push
      bots = Bot.where(deadline: 1.minute.ago..Time.current)
      token = "4ebuWYv6GddsBeceoOfwqGBCGSA3Rg2aPYTSKqjrUv0ykSa8ZwaFVFe2Y4+UIHuYwY63TGyxnkOU+IvnV7AW5SD+bz6pufZ4ITloVGycW04YwjgPL1+IyBXImEdpV/0PH8JWliEmwlumJ6KakOYYLgdB04t89/1O/w1cDnyilFU="
      client = LineClient.new(token)
      bots.each do |b|
        client.push(b.uid, b.value)
        b.destroy
      end
  end
  

  ######データベースとの照合#######
  def check(token, message, uid, user_type)
    client = LineClient.new(CHANNEL_ACCESS_TOKEN)

    # user情報取得
    user = User.find_by(uid: uid)
    # 全て取得
    bots = Bot.where(uid: uid)
    # textで検索
    bot_text = Bot.find_by(uid: uid, text: message["text"], api_id: 2)
    bot_location = Bot.find_by(uid: uid, text: message["text"], api_id: [1,4])

    ##ーーー個人ユーザの場合のリプライーーー##
    if user_type == "user"
      if message["text"] == "ls" || message["text"] == "コマンド一覧"
        texts = "---コマンド一覧---"
        bots.each do |b|
          if b.blank?
          else
            if b.api_id == 1 || b.api_id == 4
              texts << "\n位置情報 + #{b.text}"
            elsif b.api_id == 2
              texts << "\n#{b.text}"
            end
          end
        end
        res = client.reply(token, texts)
      elsif message["text"] == "web"
        res = client.reply(token, "https://mario.ht.sfc.keio.ac.jp/mariobot")

      ## 位置情報受信時の処理 ##
      elsif message["type"] == "location"
        res = client.reply(token, "か〜ら〜の？")
      elsif user.lat != 0.0
        if bot_location.blank?
          text = message["text"].gsub(" ", ",")
          res = client.reply_location(token, user.lat, user.lon, 4, text)
        else
          res = client.reply_location(token, user.lat, user.lon, bot_location.api_id, bot_location.keyword)
        end
        user.update(lat: 0, lon:0)

      ## その他 ##
      else
        ##データベース内のコマンド##
        if bot_text.blank?
          res = client.reply(token, "このコマンドに対応するものは存在しません")
        else
          res = client.reply(token, bot_text.value)
        end
      end

    ##ーーーグループもしくは複数人の場合のリプライーーー##
    else
      if message["text"] == "ls" || message["text"] == "コマンド一覧"
        texts = "---コマンド一覧---\n"
        res = client.reply(token, texts)
      elsif message["text"] == "web"
        res = client.reply(token, "https://mario.ht.sfc.keio.ac.jp/mariobot")
      else
        ##データベース内のコマンド##
        res = client.reply(token, "このコマンドに対応するものは存在しません")
      end
    end
    # if res.status == 200
    #   logger.info({success: res})
    # else
    #   logger.info({fail: res})
    # end
  end

  private
  # verify access from LINE
  def is_validate_signature
    signature = request.headers["X-LINE-Signature"]
    http_request_body = request.raw_post
    hash = OpenSSL::HMAC::digest(OpenSSL::Digest::SHA256.new, CHANNEL_SECRET, http_request_body)
    signature_answer = Base64.strict_encode64(hash)
    signature == signature_answer
  end
end

