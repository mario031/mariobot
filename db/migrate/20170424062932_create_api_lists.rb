class CreateApiLists < ActiveRecord::Migration[5.0]
  def change
    create_table :api_lists do |t|
      t.string :api
      t.string :api_key
      t.string :api_secret

      t.timestamps
    end
  end
end
